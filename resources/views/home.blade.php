@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-lg-12">

            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        DASHBOARD
                    </h3>
                </div>

                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-4">
                        <div class="card card-chart pointer" style="height: 150px;background-color: black">
                            <div class="card-body">
                                <div class="chart-area">
                                    <p class="font-weight-bolder mb-4 pt-2"
                                        style="color: rgb(255, 255, 255); text-align: center; font-weight: bold;font-size: 20px">
                                        Department Count</p>
                                    <div style="overflow: hidden;">
                                        <p style="float: left; padding-left: 35px;color: rgb(255, 255, 255);"> Count :</p>
                                        <p style="float: right; padding-right: 55px;color: rgb(255, 255, 255);">{{$companyCount}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-chart pointer" style="height: 150px;background-color: black">
                            <div class="card-body">
                                <div class="chart-area">
                                    <p class="font-weight-bolder mb-4 pt-2"
                                        style="color: rgb(255, 255, 255); text-align: center; font-weight: bold;font-size: 20px">
                                       Employee Count</p>
                                    <div style="overflow: hidden;">
                                        <p style="float: left; padding-left: 35px;color: rgb(255, 255, 255);"> Count :</p>
                                        <p style="float: right; padding-right: 55px;color: rgb(255, 255, 255);">{{$employeeCount}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-chart pointer" style="height: 150px;background-color: black">
                            <div class="card-body">
                                <div class="chart-area">
                                    <p class="font-weight-bolder mb-4 pt-2"
                                        style="color: rgb(255, 255, 255); text-align: center; font-weight: bold;font-size: 20px">
                                       User Count</p>
                                    <div style="overflow: hidden;">
                                        <p style="float: left; padding-left: 35px;color: rgb(255, 255, 255);"> Count :</p>
                                        <p style="float: right; padding-right: 55px;color: rgb(255, 255, 255);">{{$userCount}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
