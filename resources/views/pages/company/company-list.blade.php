@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        Department List
                    </h3>
                </div>
                <x-alert />
                <div class="d-flex justify-content-center" style="padding-top: 20px;">
                    {{ $companyDetails->links('pagination::bootstrap-4') }}
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Department&nbsp;Name</th>
                                <th scope="col">Department Slug</th>
                                <th scope="col">Department Create Date</th>
                                <th scope="col">Status</th>
                                <th scope="col" style="width: 50px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($companyDetails as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->id }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->fldCompanyName }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->fldSlug }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->created_at }}</p>
                                    </td>
                                    <td>
                                        @if ($item->fldStatus == 1)
                                            <span class="badge bg-success">Publish</span>
                                        @else
                                            <span class="badge bg-secondary">Draft</span>
                                        @endif
                                    </td>


                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-info dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false" style="background-color: black;color:white;">
                                                {{ __('Action') }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div>
                                                    <a class="dropdown-item"
                                                        href="{{ route('view-company-edit-form', $item->id) }}">{{ __('Edit Details') }}</a>
                                                </div>
                                                <div>
                                                    <form action="{{ route('remove-company') }}" method="POST">
                                                        @csrf
                                                        <input type="text" name="id" value="{{ $item->id }}" hidden>
                                                        <button type="submit"
                                                            style="border: none;background-color: transparent;padding-left: 25px;">
                                                            Delete Department
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12" style="text-align: center;">
                                        ....&nbsp;&nbsp;&nbsp;&nbsp;No
                                        Recode
                                        Found&nbsp;&nbsp;&nbsp;&nbsp;....</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-center" style="padding-top: 20px;">
                    {{ $companyDetails->links('pagination::bootstrap-4') }}
                </div>
            </div>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>
@endsection
