@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        Company Details Form
                    </h3>
                </div>
                <x-alert />
                <form action="{{ route('update-company-details') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <label class="form-label">Company Name</label>
                        <div class="form-group{{ $errors->has('company_name') ? ' has-danger' : '' }}">
                            <input name="company_name"
                                class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" type="text"
                                autocomplete="off" id="company_name" value="{{ $companyDetail->fldCompanyName }}">
                            @if ($errors->has('company_name'))
                                <span class="invalid feedback" role="alert">
                                    <strong style="color: rgb(253, 0, 0);">*{{ $errors->first('company_name') }}.</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <input type="text" name="id" value="{{ $companyDetail->id }}" hidden>


                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="form-label">Company Status</label>
                                <div>
                                    @if ($companyDetail->fldStatus == 1)
                                        <label style="padding-right: 20px;"><input type="radio" name="status" value="1" checked>
                                            Publish</label>
                                    @else
                                        <label style="padding-right: 20px;"><input type="radio" name="status" value="1">
                                            Publish</label>
                                    @endif
                                    @if ($companyDetail->fldStatus == 2)
                                        <label style="padding-right: 20px;"><input type="radio" name="status" value="2" checked>
                                            Draft</label>
                                    @else
                                        <label style="padding-right: 20px;"><input type="radio" name="status" value="2">
                                            Draft</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <label class="form-label">Other Information.</label>
                        <textarea class="form-control" rows="3"
                            name="company_other_details">{{ $companyDetail->fldOtherDetails }}</textarea>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div style="padding-right: 20px;">
                                <button type="submit" class="btn btn-dark" style="float: right;">Submit Form</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>


        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>

@endsection
