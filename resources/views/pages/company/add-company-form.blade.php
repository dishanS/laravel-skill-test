@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        Company Department Form
                    </h3>
                </div>
                <x-alert />
                <form action="{{ route('create-company') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <label class="form-label">Company Department Name</label>
                        <div class="form-group{{ $errors->has('company_name') ? ' has-danger' : '' }}">
                            <input name="company_name"
                                class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" type="text"
                                placeholder="HR Department" autocomplete="off" id="company_name">
                            @if ($errors->has('company_name'))
                                <span class="invalid feedback" role="alert">
                                    <strong style="color: rgb(253, 0, 0);">*{{ $errors->first('company_name') }}.</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <label class="form-label">Other Information.</label>
                        <textarea class="form-control" rows="3"
                            name="company_other_details">{{ old('company_other_details') }}</textarea>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div style="padding-right: 20px;">
                                <button type="submit" class="btn btn-dark" style="float: right;">Submit Form</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>

@endsection
