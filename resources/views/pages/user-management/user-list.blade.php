@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        User List
                    </h3>
                </div>
                <x-alert />
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">User&nbsp;Name</th>
                                <th scope="col">User&nbsp;Type</th>
                                <th scope="col">User&nbsp;Email</th>
                                <th scope="col" style="width: 50px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($allUserDetails as $item)
                                <tr>
                                    <th scope="row">{{ $item->id }}</th>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        @if ($item->fldUserRole == 1)
                                            Admin
                                        @elseif ($item->fldUserRole == null)
                                            Normal User
                                        @endif
                                    </td>
                                    <td>{{ $item->email }}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-info dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false" style="background-color: black;color:white;">
                                                {{ __('Action') }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div>
                                                    <form action="{{ route('change-user-as-admin') }}" method="POST">
                                                        @csrf
                                                        <input type="text" name="id" value="{{ $item->id }}" hidden>
                                                        <button type="submit"
                                                            style="border: none;background-color: transparent;padding-left: 25px;">
                                                            Change To Admin
                                                        </button>
                                                    </form>
                                                </div>
                                                <div>
                                                    <form action="{{ route('change-user-as-normal-user') }}" method="POST">
                                                        @csrf
                                                        <input type="text" name="id" value="{{ $item->id }}" hidden>
                                                        <button type="submit"
                                                            style="border: none;background-color: transparent;padding-left: 25px;">
                                                            Demote User
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>
@endsection
