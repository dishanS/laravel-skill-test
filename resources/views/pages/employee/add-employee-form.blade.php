@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        Employee Details Form
                    </h3>
                </div>
                <x-alert />
                <form action="{{ route('create-employee') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Employee First Name</label>
                                <div class="form-group{{ $errors->has('employee_first_name') ? ' has-danger' : '' }}">
                                    <input name="employee_first_name"
                                        class="form-control{{ $errors->has('employee_first_name') ? ' is-invalid' : '' }}"
                                        type="text" placeholder="Dishan" autocomplete="off" id="employee_first_name">
                                    @if ($errors->has('employee_first_name'))
                                        <span class="invalid feedback" role="alert">
                                            <strong
                                                style="color: rgb(253, 0, 0);">*{{ $errors->first('employee_first_name') }}.</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Employee Last Name</label>
                                <div class="form-group{{ $errors->has('employee_last_name') ? ' has-danger' : '' }}">
                                    <input name="employee_last_name"
                                        class="form-control{{ $errors->has('employee_last_name') ? ' is-invalid' : '' }}"
                                        type="text" placeholder="Himesha" autocomplete="off" id="employee_last_name">
                                    @if ($errors->has('employee_last_name'))
                                        <span class="invalid feedback" role="alert">
                                            <strong
                                                style="color: rgb(253, 0, 0);">*{{ $errors->first('employee_last_name') }}.</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Company</label>
                                <select class=" " name="employee_company_id" id="country" aria-haspopup="true"
                                    aria-expanded="false" style="width: 100%; height: 39px;">
                                    @foreach ($companyDetails as $list)
                                        <option value="{{ $list->id }}" style="color: black">
                                            {{ $list->fldCompanyName }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Employee Profile Photo</label>
                                <input type="file" class="form-control" placeholder="Himesha" name="employee_profile_pic"
                                    value="{{ old('employee_profile_pic') }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Company Email address</label>
                                <input type="email" class="form-control" placeholder="dishan@example.com"
                                    name="employee_email" value="{{ old('employee_email') }}">
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Employee Telephone</label>
                                <input type="text" class="form-control" placeholder="031xxxxxxx" name="employee_tlp"
                                    value="{{ old('employee_tlp') }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div style="padding-right: 20px;">
                                <button type="submit" class="btn btn-dark" style="float: right;">Submit Form</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>
@endsection
