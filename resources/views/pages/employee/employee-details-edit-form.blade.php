@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        Employee Details Edit Form
                    </h3>
                </div>
                <x-alert />
                <form action="{{ route('update-employee-details') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Employee First Name</label>
                                <div class="form-group{{ $errors->has('employee_first_name') ? ' has-danger' : '' }}">
                                    <input name="employee_first_name"
                                        class="form-control{{ $errors->has('employee_first_name') ? ' is-invalid' : '' }}"
                                        type="text" autocomplete="off" id="employee_first_name"
                                        value="{{ $employeeDetail->fldFirstName }}">
                                    @if ($errors->has('employee_first_name'))
                                        <span class="invalid feedback" role="alert">
                                            <strong
                                                style="color: rgb(253, 0, 0);">*{{ $errors->first('employee_first_name') }}.</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Employee Last Name</label>
                                <div class="form-group{{ $errors->has('employee_last_name') ? ' has-danger' : '' }}">
                                    <input name="employee_last_name" value="{{ $employeeDetail->fldLastName }}"
                                        class="form-control{{ $errors->has('employee_last_name') ? ' is-invalid' : '' }}"
                                        type="text"autocomplete="off" id="employee_last_name">
                                    @if ($errors->has('employee_last_name'))
                                        <span class="invalid feedback" role="alert">
                                            <strong
                                                style="color: rgb(253, 0, 0);">*{{ $errors->first('employee_last_name') }}.</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="text" name="id" value="{{ $employeeDetail->id }}" hidden>
                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Company</label>
                                <select class=" " name="employee_company_id" id="country" aria-haspopup="true"
                                    aria-expanded="false" style="width: 100%; height: 39px;">
                                    @foreach ($companyDetails as $list)
                                        <option value="{{ $list->id }}" style="color: black"
                                            {{ $list->id == $employeeDetail->fldCompanyId ? 'selected' : '' }}>
                                            {{ $list->fldCompanyName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Employee Profile Photo</label>
                                <input type="file" class="form-control" placeholder="Himesha" name="employee_profile_pic"
                                    value="{{ $employeeDetail->fldProfilePicStorageName }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Company Email address</label>
                                <input type="email" class="form-control"
                                    name="employee_email" value="{{ $employeeDetail->fldEmployeeEmail }}">
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Employee Telephone</label>
                                <input type="text" class="form-control"name="employee_tlp"
                                    value="{{ $employeeDetail->fldEmployeeTelephone }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="padding-top: 25px;">
                        <div class="row">
                            <div style="padding-right: 20px;">
                                <button type="submit" class="btn btn-dark" style="float: right;">Submit Form</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align:left;">
                        Profile Photo
                    </h3>
                    <div>
                        @if (@empty($employeeDetail->fldProfilePicStorageName))
                            <p>No Image Found</p>
                        @else
                            <img src="{{ asset('/storage/application') }}/{{ $employeeDetail->fldProfilePicStorageName }}"
                                alt="avatar" style="width: 100px; height: 100px;margin-left: 10px; ">
                        @endif
                    </div>
                    <div>
                        <form action="{{ route('delete-profile-image') }}" method="POST">
                            @csrf
                            <input type="text" name="id" value="{{ $employeeDetail->id }}" hidden>
                            <button type="submit" style="border: none;background-color: transparent">
                                <span class="badge bg-danger">Delete</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>
@endsection
