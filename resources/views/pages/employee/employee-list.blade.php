@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-body">
                <div class="row">
                    <h3 style="padding-bottom: 20px;text-align: center;">
                        Employees List
                    </h3>
                </div>
                <x-alert />
                <div class="d-flex justify-content-center" style="padding-top: 20px;">
                    {{ $employeeDetails->links('pagination::bootstrap-4') }}
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Employee&nbsp;Name</th>
                                <th scope="col">Employee&nbsp;Company</th>
                                <th scope="col">Employee&nbsp;Email&nbsp;address</th>
                                <th scope="col">Employee&nbsp;Telephone</th>
                                <th scope="col">Employee&nbsp;Profile&nbsp;Photo</th>
                                <th scope="col" style="text-align: center;width:50px;"></th>
                            </tr>
                        </thead>
                        <tbody>

                            @forelse ($employeeDetails as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->id }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->fldFirstName }}&nbsp;{{ $item->fldLastName }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item['companyName']->fldCompanyName }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs  mb-0">{{ $item->fldEmployeeEmail }}</p>
                                    </td>
                                    <td>
                                        <a href="{{ $item->fldWebsite }}">
                                            <p class="text-xs  mb-0">{{ $item->fldEmployeeTelephone }}</p>
                                        </a>
                                        {{-- <p class="text-xs  mb-0">{{ $item->fldWebsite }}</p> --}}
                                    </td>
                                    <td>
                                        @if (@empty($item->fldProfilePicStorageName))
                                            <p>No Image Found</p>
                                        @else
                                            <img src="{{ asset('/storage/application') }}/{{ $item->fldProfilePicStorageName }}"
                                                alt="avatar" style="width: 80px; height: 80px; ">
                                        @endif
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-info dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false" style="background-color: black;color:white;">
                                                {{ __('Action') }}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <div>
                                                    <a class="dropdown-item"
                                                        href="{{ route('view-employee-edit-form', $item->id) }}">{{ __('Edit Details') }}</a>
                                                </div>
                                                <div>
                                                    <form action="{{ route('remove-employee') }}" method="POST">
                                                        @csrf
                                                        <input type="text" name="id" value="{{ $item->id }}" hidden>
                                                        <button type="submit"
                                                            style="border: none;background-color: transparent;padding-left: 25px;">
                                                            Delete Employee
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12" style="text-align: center;">
                                        ....&nbsp;&nbsp;&nbsp;&nbsp;No
                                        Recode
                                        Found&nbsp;&nbsp;&nbsp;&nbsp;....</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-center" style="padding-top: 20px;">
                    {{ $employeeDetails->links('pagination::bootstrap-4') }}
                </div>
            </div>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 2000);
        </script>
    </div>
@endsection
