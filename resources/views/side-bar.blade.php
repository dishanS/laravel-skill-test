<style>
    .remove-dot {
        list-style-type: none;
    }

    .text-style {
        text-align: left;
        font-size: 20px;
        color: black;
    }

</style>
<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
    <div class="offcanvas-header" style="background-color: black">
        <h5 class="offcanvas-title" id="offcanvasExampleLabel">
            <a style="font-weight: bold; color:ghostwhite;font-size: 25px;padding-left: 25px;text-decoration: none;"
                href="{{ route('home') }}">Admin Panel</a>
        </h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
            style="background-color: ghostwhite" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">

        <li class="remove-dot">
            <a class="dropdown-item text-style" href="{{ route('add-Company-form-view') }}">Add Department</a>
        </li>
        <li class="remove-dot text-style">
            <a class="dropdown-item" href="{{ route('add-employee-form-view') }}">Add Employee</a>
        </li>

        <hr>

        <li class="remove-dot">
            <a class="dropdown-item text-style" href="{{ route('Company-list-view') }}">Department List</a>
        </li>
        <li class="remove-dot text-style">
            <a class="dropdown-item" href="{{ route('employee-list-view') }}">Employee List</a>
        </li>

        <hr>

        {{-- <li class="remove-dot text-style">
            <a class="dropdown-item" href="{{ route('user-list-view') }}">User Management</a>
        </li> --}}

        @guest
        @else
            @if (auth()->user()->fldUserRole == '1')
                <li class="remove-dot text-style">
                    <a class="dropdown-item" href="{{ route('user-list-view') }}">User Management</a>
                </li>
            @endif
        @endguest

    </div>
</div>
