<div>
    @if (session()->has('message'))
        <div class="alert " style="background-color: #3BFD00; color: cornsilk;">
            {{ session()->get('message') }}
        </div>
    @elseif(session()->has('error'))
        <div class="alert" style="background-color: #FF0909; color: cornsilk;">
            {{ session()->get('error') }}
        </div>
    @elseif(session()->has('updateSucc'))
        <div class="alert" style="background-color: #3BFD00; color: cornsilk;">
            {{ session()->get('updateSucc') }}
        </div>
    @elseif(session()->has('updateError'))
        <div class="alert" style="background-color: #FF0909; color: cornsilk;">
            {{ session()->get('updateError') }}
        </div>
    @elseif(session()->has('success'))
        <div class="alert alert-success" style="text-align: center; color: cornsilk;">
            {{ session()->get('success') }}
        </div>
    @endif

</div>
