<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanyDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'fldCompanyName' => 'We Are Designers',
            'fldEmail' => 'designers@gmail.com',
            'fldTelephone' => '0113654226',
            'fldLogoStorageName' => 'logo01.jpg',
            'fldCoverImageStorageName' => 'cover01.jpg',
            'fldWebsite' => 'https://www.wearedesigners.net/',
            'fldStatus' => '1',
            'fldOtherDetails' => 'Over the past decade, we have collaborated with many clients on national and international platforms to help them reach their highest business aptitude with the support of effective designing.',
            'fldSlug' => 'we-are-designers'
        ]);
        Company::create([
            'fldCompanyName' => 'IT Sri Lanka',
            'fldEmail' => 'it@gmail.com',
            'fldTelephone' => '0113654226',
            'fldLogoStorageName' => 'logo02.jpeg',
            'fldCoverImageStorageName' => 'cover02.jpg',
            'fldWebsite' => 'https://www.wearedesigners.net/',
            'fldStatus' => '1',
            'fldOtherDetails' => 'Over the past decade, we have collaborated with many clients on national and international platforms to help them reach their highest business aptitude with the support of effective designing.',
            'fldSlug' => 'we-are-designers'
        ]);
        Company::create([
            'fldCompanyName' => 'Space Code',
            'fldEmail' => 'space@gmail.com',
            'fldTelephone' => '0113654226',
            'fldLogoStorageName' => 'logo03.jpeg',
            'fldCoverImageStorageName' => 'cover03.jpg',
            'fldWebsite' => 'https://www.wearedesigners.net/',
            'fldStatus' => '1',
            'fldOtherDetails' => 'Over the past decade, we have collaborated with many clients on national and international platforms to help them reach their highest business aptitude with the support of effective designing.',
            'fldSlug' => 'we-are-designers'
        ]);
        Company::create([
            'fldCompanyName' => 'Nawaloka Designers',
            'fldEmail' => 'nawaloka@gmail.com',
            'fldTelephone' => '0113654226',
            'fldLogoStorageName' => 'logo04.jpeg',
            'fldCoverImageStorageName' => 'cover04.jpg',
            'fldWebsite' => 'https://www.wearedesigners.net/',
            'fldStatus' => '1',
            'fldOtherDetails' => 'Over the past decade, we have collaborated with many clients on national and international platforms to help them reach their highest business aptitude with the support of effective designing.',
            'fldSlug' => 'we-are-designers'
        ]);
        Company::create([
            'fldCompanyName' => 'Code Lanka ',
            'fldEmail' => 'code@gmail.com',
            'fldTelephone' => '0113654226',
            'fldLogoStorageName' => 'logo05.jpeg',
            'fldCoverImageStorageName' => 'cover05.jpg',
            'fldWebsite' => 'https://www.wearedesigners.net/',
            'fldStatus' => '1',
            'fldOtherDetails' => 'Over the past decade, we have collaborated with many clients on national and international platforms to help them reach their highest business aptitude with the support of effective designing.',
            'fldSlug' => 'we-are-designers'
        ]);
        Company::create([
            'fldCompanyName' => 'Zeldiva Designers',
            'fldEmail' => 'zeldiva@gmail.com',
            'fldTelephone' => '0113654226',
            'fldLogoStorageName' => 'logo06.jpeg',
            'fldCoverImageStorageName' => 'cover06.jpg',
            'fldWebsite' => 'https://www.wearedesigners.net/',
            'fldStatus' => '1',
            'fldOtherDetails' => 'Over the past decade, we have collaborated with many clients on national and international platforms to help them reach their highest business aptitude with the support of effective designing.',
            'fldSlug' => 'we-are-designers'
        ]);
    }
}
