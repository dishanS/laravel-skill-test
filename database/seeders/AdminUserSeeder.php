<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'We Are Designers',
            'email' => 'admin@wearedesigners.net',
            'email_verified_at' => now(),
            'password' => Hash::make('654321'),
            'created_at' => now(),
            'updated_at' => now(),
            'fldUserRole' =>'1'
        ]);
    }
}
