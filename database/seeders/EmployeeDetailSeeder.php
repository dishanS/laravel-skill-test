<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'fldFirstName' => 'Dishan',
            'fldLastName' => 'Himesha',
            'fldCompanyId' => '2',
            'fldEmployeeEmail' => 'dishan@gmail.com',
            'fldEmployeeTelephone' => '0778696551',
            'fldProfilePicStorageName' => 'employee01.png'
        ]);
        Employee::create([
            'fldFirstName' => 'Ashan',
            'fldLastName' => 'Somatilaka',
            'fldCompanyId' => '1',
            'fldEmployeeEmail' => 'ashan@gmail.com',
            'fldEmployeeTelephone' => '0778545221',
            'fldProfilePicStorageName' => 'employee02.png'
        ]);
        Employee::create([
            'fldFirstName' => 'Kamidu',
            'fldLastName' => 'Rantilaka',
            'fldCompanyId' => '2',
            'fldEmployeeEmail' => 'kamidu@gmail.com',
            'fldEmployeeTelephone' => '0126654256',
            'fldProfilePicStorageName' => 'employee03.jpg'
        ]);
        Employee::create([
            'fldFirstName' => 'Nipuni',
            'fldLastName' => 'Dulanjalee',
            'fldCompanyId' => '6',
            'fldEmployeeEmail' => 'nipuni@gmail.com',
            'fldEmployeeTelephone' => '0778455214',
            'fldProfilePicStorageName' => 'employee04.png'
        ]);
        Employee::create([
            'fldFirstName' => 'Dishemi',
            'fldLastName' => 'Sashinika',
            'fldCompanyId' => '5',
            'fldEmployeeEmail' => 'Sashinika@gmail.com',
            'fldEmployeeTelephone' => '0112564223',
            'fldProfilePicStorageName' => 'employee05.png'
        ]);
        Employee::create([
            'fldFirstName' => 'Ransika',
            'fldLastName' => 'Samaradiwakara',
            'fldCompanyId' => '1',
            'fldEmployeeEmail' => 'ransika@gmail.com',
            'fldEmployeeTelephone' => '0778541256',
            'fldProfilePicStorageName' => 'employee06.png'
        ]);
    }
}
