<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblCompany', function (Blueprint $table) {
            $table->id();
            $table->string('fldCompanyName', 500);
            $table->string('fldEmail', 500)->nullable();
            $table->string('fldTelephone', 50)->nullable();
            $table->longText('fldLogoStorageName')->nullable();
            $table->longText('fldCoverImageStorageName')->nullable();
            $table->string('fldWebsite', 500)->nullable();
            $table->string('fldStatus', 50)->nullable();
            $table->string('fldSlug', 500)->nullable();
            $table->longText('fldOtherDetails')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblCompany');
    }
}
