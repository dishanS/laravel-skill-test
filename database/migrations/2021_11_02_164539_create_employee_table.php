<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblEmployee', function (Blueprint $table) {
            $table->id();
            $table->string('fldFirstName', 500);
            $table->string('fldLastName', 50);
            $table->string('fldCompanyId', 50)->nullable();
            $table->string('fldEmployeeEmail', 50)->nullable();
            $table->string('fldEmployeeTelephone', 50)->nullable();
            $table->longText('fldProfilePicStorageName')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblEmployee');
    }
}
