<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userListViewPage()
    {
        $allUserDetails = User::all();
        return view('pages.user-management.user-list')->with(
            [
                'allUserDetails' => $allUserDetails
            ]
        );
    }

    public function makeUserAsAdmin(Request $request)
    {
        $id = $request->id;
        $userRole = 1;
        $userDetails = User::where('id', $id)->first();
        $userDetails->update(
            [
                'fldUserRole' => $userRole,
            ]
        );

        return redirect()->back()->with('success', 'Profile Image Remove successfully.');
    }

    public function makeUserAsNormalUser(Request $request)
    {
        $id = $request->id;
        $userRole = null;
        $userDetails = User::where('id', $id)->first();
        $userDetails->update(
            [
                'fldUserRole' => $userRole,
            ]
        );

        return redirect()->back()->with('success', 'Profile Image Remove successfully.');
    }
}
