<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewEmployeeDetailsInsertForm()
    {
        $companyList = Company::all();
        return view('pages.employee.add-employee-form')->with(
            [
                'companyDetails' => $companyList
            ]
        );
    }

    public function employeeListViewPage()
    {
        $employeeDetails = Employee::with(['companyName'])->orderBy('created_at', 'DESC')->paginate(5);
        return view('pages.employee.employee-list')->with(
            [
                'employeeDetails' => $employeeDetails
            ]
        );
    }

    public function storEmployee(EmployeeRequest $request)
    {
        if (empty($request->hasFile('employee_profile_pic'))) {
            $storageProfileImageName = null;
        } else {
            #store logo Image
            $employeeProfileImageStorageRandomId = md5(uniqid(rand(), true));
            $originalProfileName = $request->employee_profile_pic->getClientOriginalName();
            $storageProfileImageName = $employeeProfileImageStorageRandomId . '-' . $originalProfileName;

            $request->employee_profile_pic->move(public_path('storage/application'), $storageProfileImageName);
        }

        Employee::create(
            [
                'fldFirstName' => $request['employee_first_name'],
                'fldLastName' => $request['employee_last_name'],
                'fldCompanyId' => $request['employee_company_id'],
                'fldEmployeeEmail' => $request['employee_email'],
                'fldEmployeeTelephone' => $request['employee_tlp'],
                'fldProfilePicStorageName' => $storageProfileImageName
            ]
        );

        return redirect()->back()->with('success', 'Employee Create successfully.');
    }

    public function viewEmployeeDetailsEditForm($id)
    {
        #get employee details
        $employeeDetail = Employee::where('id', $id)->with(['companyName'])->first();
        $companyList = Company::all();

        return view('pages.employee.employee-details-edit-form')->with(
            [
                'employeeDetail' => $employeeDetail,
                'companyDetails' => $companyList
            ]
        );
    }

    public function removeProfileImage(Request $request)
    {
        $id = $request->id;
        $employeeDetails = Employee::find($id);
        Storage::delete(public_path('storage/application'), $employeeDetails->fldProfilePicStorageName);
        $employeeDetails->update(
            [
                'fldProfilePicStorageName' => null,
            ]
        );

        return redirect()->back()->with('success', 'Profile Image Remove successfully.');
    }

    public function updateEmployeeDetails(EmployeeRequest $request)
    {
        $id = $request->id;
        $employeeDetails = Employee::find($id);

        if (empty($request->hasFile('employee_profile_pic'))) {
            $id = $request->id;
            $employeeDetails = Employee::find($id);
            $pic = $employeeDetails->fldProfilePicStorageName;
            if (empty($pic)) {
                $storageProfileImageName = null;
            } else {
                $storageProfileImageName = $pic;
            }
        } else {
            #store logo Image
            $employeeProfileImageStorageRandomId = md5(uniqid(rand(), true));
            $originalProfileName = $request->employee_profile_pic->getClientOriginalName();
            $storageProfileImageName = $employeeProfileImageStorageRandomId . '-' . $originalProfileName;

            $request->employee_profile_pic->move(public_path('storage/application'), $storageProfileImageName);
        }

        $employeeDetails->update(
            [
                'fldFirstName' => $request['employee_first_name'],
                'fldLastName' => $request['employee_last_name'],
                'fldCompanyId' => $request['employee_company_id'],
                'fldEmployeeEmail' => $request['employee_email'],
                'fldEmployeeTelephone' => $request['employee_tlp'],
                'fldProfilePicStorageName' => $storageProfileImageName
            ]
        );

        $employeeDetails = Employee::with(['companyName'])->orderBy('created_at', 'DESC')->paginate(5);

        return redirect('/employee-list')->with(
            [
                'employeeDetails' => $employeeDetails
            ]
        );
    }

    public function removeEmployee(Request $request)
    {
        $id = $request->id;
        $employeeDetails = Employee::find($id);
        $employeeDetails->delete();

        return redirect()->back()->with('success', 'Employee Delete successfully.');
    }
}
