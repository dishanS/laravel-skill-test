<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewCompanyDetailsInsertForm()
    {
        return view('pages.company.add-company-form');
    }

    public function companyListViewPage()
    {
        $companyAllDetails = Company::orderBy('created_at', 'DESC')->paginate(5);
        return view('pages.company.company-list')->with(
            [
                'companyDetails' =>  $companyAllDetails
            ]
        );
    }

    public function storCompany(CompanyRequest $request)
    {

//        if (empty($request->hasFile('company_cover_image'))) {
//            $storageCoverImageName = null;
//        } else {
//            #store logo Image
//            $companyCoverImageStorageRandomId = md5(uniqid(rand(), true));
//            $originalCoverName = $request->company_cover_image->getClientOriginalName();
//            $storageCoverImageName = $companyCoverImageStorageRandomId . '-' . $originalCoverName;
//
//            $request->company_cover_image->move(public_path('storage/application'), $storageCoverImageName);
//        }
//
//        if (empty($request->hasFile('company_logo'))) {
//            $storageLogoImageName = null;
//        } else {
//            #store Cover Image
//            $companyLogoImageStorageRandomId = md5(uniqid(rand(), true));
//            $originalLogoName = $request->company_logo->getClientOriginalName();
//            $storageLogoImageName = $companyLogoImageStorageRandomId . '-' . $originalLogoName;
//
//            $request->company_logo->move(public_path('storage/application'), $storageLogoImageName);
//        }

        $status = 1;  #active
        $slug = str_replace(' ', '-',  $request->company_name); #slug

        $storageCoverImageName = null;
        $storageLogoImageName = null;
        $url = null;
        $tpl = null;
        $email = null;
        Company::create(
            [
                'fldCompanyName' => $request['company_name'],
                'fldEmail' => $email,
                'fldTelephone' => $tpl,
                'fldLogoStorageName' => $storageLogoImageName,
                'fldCoverImageStorageName' => $storageCoverImageName,
                'fldWebsite' => $url,
                'fldStatus' => $status,
                'fldOtherDetails' => $request['company_other_details'],
                'fldSlug' => $slug
            ]
        );

        return redirect()->back()->with('success', 'Company Create successfully.');
    }
    public function viewCompanyDetailsEditForm($id)
    {
        #get company details

        $companyDetail = Company::where('id', $id)->first();

        return view('pages.company.edit-company-details')->with(
            [
                'companyDetail' => $companyDetail,
            ]
        );
    }

    public function removeCoverImage(Request $request)
    {
        $id = $request->id;
        $companyDetails = Company::find($id);
        Storage::delete(public_path('storage/application'), $companyDetails->fldCoverImageStorageName);
        $companyDetails->update(
            [
                'fldCoverImageStorageName' => null,
            ]
        );

        return redirect()->back()->with('success', 'Cover Image Remove successfully.');
    }

    public function removeLogoImage(Request $request)
    {
        $id = $request->id;
        $companyDetails = Company::find($id);
        Storage::delete(public_path('storage/application'), $companyDetails->fldLogoStorageName);
        $companyDetails->update(
            [
                'fldLogoStorageName' => null,
            ]
        );

        return redirect()->back()->with('success', 'Logo Image Remove successfully.');
    }

    public function updateCompanyDetails(CompanyRequest $request)
    {
        if (empty($request->hasFile('company_cover_image'))) {
            $id = $request->id;
            $companyDetails = Company::find($id);
            $cover = $companyDetails->fldCoverImageStorageName;
            if (empty($cover)) {
                $storageCoverImageName = null;
            } else {
                $storageCoverImageName = $cover;
            }
        } else {
            #store logo Image
            $companyCoverImageStorageRandomId = md5(uniqid(rand(), true));
            $originalCoverName = $request->company_cover_image->getClientOriginalName();
            $storageCoverImageName = $companyCoverImageStorageRandomId . '-' . $originalCoverName;

            $request->company_cover_image->move(public_path('storage/application'), $storageCoverImageName);
        }

        if (empty($request->hasFile('company_logo'))) {
            $id = $request->id;
            $companyDetails = Company::find($id);
            $logo = $companyDetails->fldLogoStorageName;
            if (empty($logo)) {
                $storageLogoImageName = null;
            } else {
                $storageLogoImageName = $logo;
            }
        } else {
            #store Cover Image
            $companyLogoImageStorageRandomId = md5(uniqid(rand(), true));
            $originalLogoName = $request->company_logo->getClientOriginalName();
            $storageLogoImageName = $companyLogoImageStorageRandomId . '-' . $originalLogoName;

            $request->company_logo->move(public_path('storage/application'), $storageLogoImageName);
        }

        $slug = str_replace(' ', '-',  $request->company_name); #slug

        $id = $request->id;
        $companyDetails = Company::find($id);

        $companyDetails->update(
            [
                'fldCompanyName' => $request['company_name'],
                'fldEmail' => $request['company_email'],
                'fldTelephone' => $request['company_tlp'],
                'fldLogoStorageName' => $storageLogoImageName,
                'fldCoverImageStorageName' => $storageCoverImageName,
                'fldWebsite' => $request['company_url'],
                'fldStatus' => $request['status'],
                'fldOtherDetails' => $request['company_other_details'],
                'fldSlug' => $slug
            ]
        );

        $companyAllDetails = Company::orderBy('created_at', 'DESC')->paginate(5);
        return redirect('/company-list')->with(
            [
                'companyDetails' =>  $companyAllDetails
            ]
        );
    }

    public function removeCompany(Request $request)
    {
        $id = $request->id;
        $companyDetails = Company::find($id);
        $companyDetails->delete();

        return redirect()->back()->with('success', 'Company Delete successfully.');
    }
}
