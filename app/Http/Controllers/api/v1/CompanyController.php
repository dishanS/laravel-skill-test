<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Carbon\Exceptions\Exception;
use Illuminate\Http\Request;


class CompanyController extends Controller
{
    public function getCompanyDetails()
    {
        try {
            $companyDetails = Company::orderBy('created_at', 'DESC')->get();
            foreach ($companyDetails as $key => $item) {
                $result_data[$key]['id'] = $item->id;
                $result_data[$key]['company_name'] = $item->fldCompanyName;
                $result_data[$key]['email'] = $item->fldEmail;
                $result_data[$key]['Tlp'] = $item->fldTelephone;
                $result_data[$key]['web_url'] = $item->fldWebsite;
                $result_data[$key]['created_at'] = $item->created_at;
                $result_data[$key]['company_logo_image'] = asset('storage/application/' . $item->fldLogoStorageName);
                $result_data[$key]['company_cover_image'] = asset('storage/application/' . $item->fldCoverImageStorageName);
                $result_data[$key]['other_data'] = $item->fldOtherDetails;
            }

            return response([
                'success' => true,
                'message' => "done",
                'data' => $result_data
            ]);
        } catch (Exception $exception) {
            return response([
                'success' => false,
                'message' => $exception
            ]);
        }
    }
}
