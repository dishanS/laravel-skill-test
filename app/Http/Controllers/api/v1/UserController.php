<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validation =  Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            if ($validation->fails()) {
                return response()->json($validation->errors(), 202);
            }

            $userDetails = $request->all();
            $userDetails['password'] = Hash::make($userDetails['password']);

            $userDetails = User::create($userDetails);

            $resultArray = [];
            $resultArray['token'] = $userDetails->createToken('api-application')->accessToken;
            $resultArray['name'] = $userDetails->name;

            return response([
                'success' => true,
                'message' => "Register Successfully.",
                'data' => $resultArray
            ]);
        } catch (Exception $exception) {
            return response([
                'success' => false,
                'message' => $exception
            ]);
        }
    }

    public function login(Request $request)
    {
        try {
            if (Auth::attempt([
                'email' => $request->email,
                'password' => $request->password
            ])) {

                /** @var \App\Models\User $userDetails **/
                $userDetails = Auth::user();

                $resultArray = [];
                $resultArray['token'] = $userDetails->createToken('api-application')->accessToken;
                $resultArray['name'] = $userDetails->name;
            }
            return response([
                'success' => true,
                'message' => "Login Successfully.",
                'data' => $resultArray
            ]);
        } catch (Exception $exception) {
            return response([
                'success' => false,
                'message' => "Unauthorized Access."
            ]);
        }
    }

    public function logout(Request $request)
    {

        try {
            $token = $request->user()->token();
            $token->revoke();

            return response([
                'success' => true,
                'message' => "Log Out Successfully",
            ]);
        } catch (Exception $exception) {
            return response([
                'success' => false,
                'message' => $exception
            ]);
        }
    }

    public function changeUserToAdmin(Request $request)
    {
        try {

            $id = $request->user_id;
            $userRole = 1;
            $userDetails = User::where('id', $id)->first();
            $userDetails->update(
                [
                    'fldUserRole' => $userRole,
                ]
            );

            return response([
                'success' => true,
                'message' => "Update Successfully",
                'data' => $id
            ]);
        } catch (Exception $exception) {
            return response([
                'success' => false,
                'message' => $exception,
            ]);
        }
    }
}
