<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function getEmployeesDetails()
    {
        
        try {
            $employeeDetails = Employee::with(['companyName'])->orderBy('created_at', 'DESC')->get();
            foreach ($employeeDetails as $key => $item) {
                $result_data[$key]['id'] = $item->id;
                $result_data[$key]['first_name'] = $item->fldFirstName;
                $result_data[$key]['last_name'] = $item->fldLastName;
                $result_data[$key]['email'] = $item->fldEmployeeEmail;
                $result_data[$key]['Tlp'] = $item->fldEmployeeTelephone;
                $result_data[$key]['created_at'] = $item->created_at;
                $result_data[$key]['company_name'] = $item['companyName']->fldCompanyName;
                $result_data[$key]['profile_pic'] = asset('storage/application/' . $item->fldProfilePicStorageName);
            }

            return response([
                'success' => true,
                'message' => "done",
                'data' => $result_data
            ]);
        } catch (Exception $exception) {
            return response([
                'success' => false,
                'message' => $exception
            ]);
        }
    }
}
