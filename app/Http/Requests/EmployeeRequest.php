<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_first_name' => 'required',
            'employee_last_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'employee_first_name.required' => 'Employee First Name is Required',
            'employee_last_name.required' => 'Employee Last Name is Required',
        ];
    }
}
