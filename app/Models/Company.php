<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'tblCompany';
    public $primaryKey = 'id';
    protected $fillable      = [
        'fldCompanyName',
        'fldEmail',
        'fldTelephone',
        'fldLogoStorageName',
        'fldCoverImageStorageName',
        'fldWebsite',
        'fldStatus',
        'fldOtherDetails',
        'fldSlug'

    ];
}
