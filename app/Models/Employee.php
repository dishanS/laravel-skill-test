<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'tblEmployee';
    public $primaryKey = 'id';
    protected $fillable      = [
        'fldFirstName',
        'fldLastName',
        'fldCompanyId',
        'fldEmployeeEmail',
        'fldEmployeeTelephone',
        'fldProfilePicStorageName',

    ];

    public function companyName(){ 
        return $this->hasOne(Company::class,'id','fldCompanyId');
    }
}
