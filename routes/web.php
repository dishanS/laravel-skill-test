<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\UserManagementController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main-page');
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('company-form', [CompanyController::class, 'viewCompanyDetailsInsertForm'])->name('add-Company-form-view');
Route::get('company-list', [CompanyController::class, 'companyListViewPage'])->name('Company-list-view');
Route::post('store-company', [CompanyController::class, 'storCompany'])->name('create-company');
Route::get('company-edit-form/{id}', [CompanyController::class, 'viewCompanyDetailsEditForm'])->name('view-company-edit-form');
Route::post('delete-cover-pic', [CompanyController::class, 'removeCoverImage'])->name('delete-cover-image');
Route::post('delete-logo-pic', [CompanyController::class, 'removeLogoImage'])->name('delete-logo-image');
Route::post('update-company-details', [CompanyController::class, 'updateCompanyDetails'])->name('update-company-details');
Route::post('remove-company', [CompanyController::class, 'removeCompany'])->name('remove-company');


Route::get('employee-form', [EmployeeController::class, 'viewEmployeeDetailsInsertForm'])->name('add-employee-form-view');
Route::get('employee-list', [EmployeeController::class, 'employeeListViewPage'])->name('employee-list-view');
Route::get('employee-edit-form/{id}', [EmployeeController::class, 'viewEmployeeDetailsEditForm'])->name('view-employee-edit-form');
Route::post('store-employee', [EmployeeController::class, 'storEmployee'])->name('create-employee');
Route::post('delete-profile-pic', [EmployeeController::class, 'removeProfileImage'])->name('delete-profile-image');
Route::post('update-employee-details', [EmployeeController::class, 'updateEmployeeDetails'])->name('update-employee-details');
Route::post('remove-employee', [EmployeeController::class, 'removeEmployee'])->name('remove-employee');

Route::get('user-list', [UserManagementController::class, 'userListViewPage'])->name('user-list-view');
Route::post('user-update-admin', [UserManagementController::class, 'makeUserAsAdmin'])->name('change-user-as-admin');
Route::post('user-demote', [UserManagementController::class, 'makeUserAsNormalUser'])->name('change-user-as-normal-user');

