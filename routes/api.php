<?php

use App\Http\Controllers\api\v1\CompanyController;
use App\Http\Controllers\api\v1\EmployeeController;
use App\Http\Controllers\api\v1\UserController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


#api using laravel passport

Route::post('/v1/login',[UserController::class,'login']);
Route::post('/v1/register',[UserController::class,'register']);

$router->group(['middleware' => 'auth:api'], function () use ($router) {

    Route::post('/v1/logout',[UserController::class,'logout']);
    Route::get('/v1/employees-details',[EmployeeController::class,'getEmployeesDetails']);
    Route::get('/v1/company-details',[CompanyController::class,'getCompanyDetails']);
    Route::post('/v1/make-admin',[UserController::class,'changeUserToAdmin']);

});